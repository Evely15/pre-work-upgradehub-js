// Ejercicio1js
function miFuncionDeComparar(numero1, numero2) {
    // Recuerda completar los condicionales 'if'

    if (numero1 === 5) {
        console.log("numero1 es estrictamente igual a 5");
    }

    if (numero1 <= numero2) {
        console.log("numero1 no es mayor que numero2");
    }
    if (numero2 >= 0) {
        console.log("numero2 es positivo");
    }
    if (numero1 < 0) {
        console.log("numero1 es negativo o distinto de cero");
    }
}

// Prueba con distintos números, por ejemplo con 5 y 2
miFuncionDeComparar(5, 2);

// Ejercio 2
function cambioDeRueda(diametro) {
    if (diametro <= 10) {
        console.log("es una rueda para un juguete pequeño");
    } else if (diametro > 10 && diametro < 20) {
        console.log("es una rueda para un juguete mediano");
    } else if (diametro >= 20) {
        console.log("es una rueda para un juguete grande")
    }
}
cambioDeRueda(10)

// Ejercio 3 js
function creadorDeDni(num, letra) {
    var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];
    var resto = num % 23

    if (num < 0 || num > 99999999) {
        console.log('el número proporcionado no es válido')
    } else {
        if (letra === letras[resto]) {
            console.log('DNI valido')

        } else {
            console.log('DNI incorrecto')
        }
    }


}
/* var numero = prompt()
var letra = prompt() 
creadorDeDni(numero, letra) */

// Ejercio 4
function laPiramide() {
    var numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, ];
    for (let index = 0; index < numeros.length; index++) {
        const element = numeros[index];
        let fila = ''
        for (let i = 0; i < element; i++) {
            fila = fila + element

        }
        console.log(fila)
    }
}
laPiramide()

// Ejercio 5
function marvelMola() {
    var texto = "marvel mola!";
    var newtext = ''

    for (let index = 0; index < texto.length; index++) {
        const element = texto[index];

        if (index === texto.length - 1) {
            newtext = newtext + element
        } else {
            newtext = newtext + element + '-'
        }

    }
    console.log(newtext)

}
marvelMola()

// Ejercio 6 js
function jugandoPiedraPapelOTigera() {
    var jugados = 0;
    var ganados = 0;
    var perdidos = 0;

    // while ("Jugar de nuevo") {
    var jugada = prompt("Escribe \"piedra\", \"papel\" o \"tijera\"");

    // GUARDAMOS UN NÚMERO ALEATORIO DE 1 AL 9
    var aleatorio = Math.floor((Math.random() * 9) + 1);
    var elige = "";
    if (aleatorio <= 3) {
        elige = "piedra";
    } else if (aleatorio <= 6) {
        elige = "papel";
    } else {
        elige = "tijera";
    }

    // De aqui en adelante, rellena las plantillas de CODE con lo adecuado ;)
    if ((jugada == "piedra") && (elige == "piedra")) {
        alert(jugada + "-" + elige + ": Empate");
    } else if ((jugada == "piedra") && (elige == "papel")) {
        alert(jugada + "-" + elige + ": Gana PC");
        perdidos++;
    } else if ((jugada == "piedra") && (elige == "tijera")) {
        alert(jugada + "-" + elige + ": Gana Jugador");
        ganados++;
    } else if ((jugada == "papel") && (elige == "papel")) {
        alert(jugada + "-" + elige + ": Empate");
    } else if ((jugada == "papel") && (elige == "tijera")) {
        alert(jugada + "-" + elige + ": Gana PC");
        perdidos++;
    } else if ((jugada == "papel") && (elige == "piedra")) {
        alert(jugada + "-" + elige + ": Gana Jugador");
        ganados++;
    } else if ((jugada == "tijera") && (elige == "tijera")) {
        alert(jugada + "-" + elige + ": Empate");
    } else if ((jugada == "tijera") && (elige == "piedra")) {
        alert(jugada + "-" + elige + ": Gana PC");
        perdidos++;
    } else if ((jugada == "tijera") && (elige == "papel")) {
        alert(jugada + "-" + elige + ": Gana Jugador");
        ganados++;
    }
    jugados++;


    document.write("Partidas jugadas: " + jugados + "<br>");
    document.write("Partidas ganadas: " + ganados + "<br>");
    document.write("Partidas perdidas: " + perdidos + "<br>");
    document.write("Partidas empatadas: " + (jugados - ganados - perdidos));
    // };

};


// jugandoPiedraPapelOTigera()